//
//  OTEventDispatcher.h
//  TestObjCPPBlocks
//
//  Created by Olivier THIERRY on 12/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTEventDispatcher : NSObject
{
    NSMutableDictionary *_events;
}

#if NS_BLOCKS_AVAILABLE

- (void) setEvent:(id)block forKey:(NSString*)key;

- (void) fire:(NSString*)event;
- (void) fire:(NSString*)event withObject:(id)object;
- (void) fire:(NSString*)event launcher:(void(^)(id event))launcher;

#endif

@end
