//
//  OTEventDispatcher.m
//  TestObjCPPBlocks
//
//  Created by Olivier THIERRY on 12/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OTEventDispatcher.h"

@implementation OTEventDispatcher

- (id)init
{
    self = [super init];
    
    if (self) 
    {
        _events = [NSMutableDictionary new];
    }
    
    return self;
}

#if NS_BLOCKS_AVAILABLE

- (void) setEvent:(id)block forKey:(NSString*)key
{
    [_events setObject:[[block copy] autorelease] 
                forKey:key];
}

- (void) fire:(NSString*)event
{
    [self fire:event withObject:nil];
}

- (void) fire:(NSString*)event withObject:(id)object
{
    id block = [_events objectForKey:event];
    
    if (block)
    {
        ((void (^)(id)) block)(object);
    }
}

- (void) fire:(NSString*)event launcher:(void(^)(id event))launcher
{
    id block = [_events objectForKey:event];
    
    if (block)
    {
        launcher(block);
    }
}

#endif

- (void)dealloc
{    
    [_events release], _events = nil;

    [super dealloc];
}

@end
